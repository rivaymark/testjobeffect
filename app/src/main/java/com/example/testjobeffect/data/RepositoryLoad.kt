package com.example.testjobeffect.data

import com.example.testjobeffect.data.web.WebDataSource
import com.example.testjobeffect.entities.web.Product
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RepositoryLoad @Inject constructor(
    private val webUnsplash: WebDataSource,
    //private val room: RoomDataSource
) {

    suspend fun getLatest():List<Product> = webUnsplash.getLatest().latest

    suspend fun getFlashSale():List<Product.ProductWithDiscount>  = webUnsplash.getFlashSale().flashSale

}

