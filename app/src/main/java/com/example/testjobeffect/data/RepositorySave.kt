package com.example.testjobeffect.data

import com.example.testjobeffect.data.room.RoomDataSource
import com.example.testjobeffect.data.shared_pref.SharedPrefApp
import com.example.testjobeffect.entities.aouth.AppAuthState
import com.example.testjobeffect.entities.aouth.Person
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RepositorySave @Inject constructor(
    private val sp: SharedPrefApp,
    private val room:RoomDataSource
){

    fun getStateAuth() = sp.getStateAuth()

    fun saveStateAuth(state: AppAuthState){
        sp.clearStateAuth()
        sp.saveStateAuth(state)
    }

    suspend fun foundAuthorized() = room.foundAuthorized()
    suspend fun signIn(firstName:String,lastName:String,email:String) = room.signIn(firstName, lastName, email)
    suspend fun logIn(firstName:String,password:String) = room.logIn(firstName,password)
    suspend fun insert(data: Person.PersonDataAuth) = room.insert(data)
    suspend fun update(data: Person.PersonDataAuth) = room.update(data)

}