package com.example.testjobeffect.data.room

import com.example.testjobeffect.entities.aouth.Person
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RoomDataSource @Inject constructor(private val dao: RoomDAO){

    //todo мейби как то поуневерсальному но ведь что-то в отделно взятом может поменяться ????

    suspend fun signIn(firstName:String,lastName:String,email:String): Person.PersonDataAuth? {
       val person = dao.signIn(firstName, lastName, email).firstOrNull() ?: return null
       person.password = person.password.decryptCode()
       return person as Person.PersonDataAuth
    }

    suspend fun logIn(firstName:String,password:String): Person.PersonDataAuth? {
        val person = dao.logIn(firstName, password.encryptCode()).firstOrNull() ?: return null
        person.password = person.password.decryptCode()
        return person as Person.PersonDataAuth
    }

    suspend fun foundAuthorized(): Person.PersonDataAuth? {
        val person = dao.foundAuthorized(true).firstOrNull() ?: return null
        person.password = person.password.decryptCode()
        return person as Person.PersonDataAuth
    }

    suspend fun insert(data: Person.PersonDataAuth) {
        val person = RoomDBOPerson(
            id = data.id,
            firstName = data.firstName,
            lastName = data.lastName,
            email = data.email,
            password = data.password.encryptCode(),
            authorizedApp = data.authorizedApp
        )
        dao.insert(person)
    }

    suspend fun update(data: Person.PersonDataAuth) {
        val person = RoomDBOPerson(
            id = data.id,
            firstName = data.firstName,
            lastName = data.lastName,
            email = data.email,
            password = data.password.encryptCode(),
            authorizedApp = data.authorizedApp
        )
        dao.update(person)
    }

    companion object{
        // Ну так условно ( хотя в идеале надо наверное все данные ( но пока только пароль)
        fun String.encryptCode():String {
            val temp = this.map { char-> char + 1}
            var result: String = ""
            temp.forEach {
                result += it
            }
            return result
        }

        fun String.decryptCode():String {
            val temp =  this.map { char-> char - 1 }
            var result: String = ""
            temp.forEach {
                result += it
            }
            return result
        }

    }
}
