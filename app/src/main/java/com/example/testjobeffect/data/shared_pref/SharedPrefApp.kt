package com.example.testjobeffect.data.shared_pref

import android.content.SharedPreferences
import com.example.testjobeffect.entities.aouth.AppAuthState
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

//@Singleton
class SharedPrefApp @Inject constructor(
    @Named("SharedPref") private val sp:SharedPreferences,
) {

    fun saveStateAuth(state: AppAuthState)= sp.edit().putInt(KEY_STATE_AUTH,state.ordinal).apply()

    fun clearStateAuth()= sp.edit().remove(KEY_STATE_AUTH).apply()

    fun getStateAuth() = checkType(sp.getInt(KEY_STATE_AUTH, AppAuthState.NO_AUTHENTICATION.ordinal))

    private fun checkType(number:Int): AppAuthState{
        return try {
            AppAuthState.values()
                .first { type -> type.ordinal == number }
        }
        catch (e: NoSuchElementException ){AppAuthState.NO_AUTHENTICATION}
    }

  companion object{
      const val KEY_STATE_AUTH = "KEY_STATE_AUTH_APP_8fsdrmih"
  }
}