package com.example.testjobeffect.data.web

import retrofit2.Response
import retrofit2.http.*

interface WebApiServices {


    @GET("https://run.mocky.io/v3/a9ceeb6e-416d-4352-bde6-2203416576ac")
    suspend fun getFlashSale(): Response<WebDTOQueryProductWithDiscount>


    @GET("https://run.mocky.io/v3/cc0071a1-f06e-48fa-9e90-b1c2a61eaca7")
    suspend fun getLatest():Response<WebDTOQueryProduct>

}