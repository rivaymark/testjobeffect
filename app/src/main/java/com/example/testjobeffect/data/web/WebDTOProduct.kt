package com.example.testjobeffect.data.web

import com.example.testjobeffect.entities.web.Product
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@Json
data class WebDTOProduct (
    @Json(name ="category") override val category: String,
    @Json(name ="name") override val name: String,
    @Json(name ="price") override val price: Float,
    @Json(name ="image_url") override val imageUrl: String
):Product

@JsonClass(generateAdapter = true)
data class WebDTOQueryProduct (
    @Json(name ="latest") var latest: List<WebDTOProduct>
)

