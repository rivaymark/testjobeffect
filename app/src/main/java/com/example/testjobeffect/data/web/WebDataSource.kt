package com.example.testjobeffect.data.web

import com.example.testjobeffect.entities.web.StateQuery
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

@Singleton
class WebDataSource @Inject constructor(@Named("ApiService") private val apiServices: WebApiServices) {

    suspend fun getLatest() = checkCodeResponse(apiServices.getLatest())

    suspend fun getFlashSale() = checkCodeResponse(apiServices.getFlashSale())

    companion object {
        fun <T> checkCodeResponse(response: Response<T>) = when (response.code()) {
            StateQuery.SomethingWrongServer.code -> throw Exception(StateQuery.SomethingWrongServer.message)
            StateQuery.NotCorrectQuery.code -> throw Exception(StateQuery.NotCorrectQuery.message)
            StateQuery.NotCorrectScope.code -> throw Exception(StateQuery.NotCorrectScope.message)
            StateQuery.NotToken.code -> throw Exception(StateQuery.NotToken.message)
            StateQuery.NotResources.code -> throw Exception(StateQuery.NotResources.message)
            StateQuery.OK.code -> response.body() ?: throw Exception(StateQuery.OK.messageForNull)
            else -> throw Exception(StateQuery.HZ.message) //под смыслом хз , хто знает?! -> хз
        }
    }
}