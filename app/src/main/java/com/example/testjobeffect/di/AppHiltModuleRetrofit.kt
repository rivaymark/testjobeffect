package com.example.testjobeffect.di

import com.example.testjobeffect.data.web.WebApiServices
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppHiltModuleRetrofit {

    /*****************************************************************************/
    /****                           Retrofit                               * *****/
    /*****************************************************************************/
    @Named("BASE_URL")
    @Provides
    fun providesBaseUrl() = "https://run.mocky.io/"

    @Named("Moshi")
    @Provides
    fun providesMoshi()= Moshi.Builder().add(KotlinJsonAdapterFactory()).build()

    /*@Named("OkHttpClient")
    @Provides
    @Singleton
    fun provideOkHttp(): OkHttpClient =
        OkHttpClient
            .Builder()
            .build()*/

    @Named("Retrofit")
    @Singleton
    @Provides
    fun providesRetrofit(
        @Named("Moshi") moshi: Moshi,
        @Named("BASE_URL") url: String
    ) = Retrofit.Builder()
        .baseUrl(url)
        .addConverterFactory(MoshiConverterFactory.create(moshi))  // сериализатор
        //.client(okHttp)
        .build()

    @Named("ApiService")
    @Singleton
    @Provides
    fun providesApiService(@Named("Retrofit") retrofit: Retrofit ) = retrofit.create(WebApiServices::class.java)
}