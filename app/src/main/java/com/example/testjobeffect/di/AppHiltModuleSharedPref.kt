package com.example.testjobeffect.di

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Named

@Module
@InstallIn(SingletonComponent::class)
object AppHiltModuleSharedPref {

    /*****************************************************************************/
    /****                            SHARED PREF                           * *****/
    /*****************************************************************************/

    @Named("NameSharedPref")
    @Provides
    fun providesNameSharedPref() = "SharedPrefApp"


    @Named("SharedPref")
    @Provides
    fun providesSharedPref(
        @Named("NameSharedPref") name: String,
        @ApplicationContext context: Context
    ) = context.getSharedPreferences(name, Context.MODE_PRIVATE)

}