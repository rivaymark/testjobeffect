package com.example.testjobeffect.domain.use_case_auth


import com.example.testjobeffect.data.RepositorySave
import com.example.testjobeffect.entities.aouth.Person
import javax.inject.Inject

class UseCaseLogOut @Inject constructor(private val repository: RepositorySave) {

    suspend fun update(data: Person.PersonDataAuth) = repository.update(data)
}