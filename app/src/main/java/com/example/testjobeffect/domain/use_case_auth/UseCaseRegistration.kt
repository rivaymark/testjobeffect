package com.example.testjobeffect.domain.use_case_auth

import com.example.testjobeffect.data.RepositorySave
import com.example.testjobeffect.entities.aouth.Person
import javax.inject.Inject

class UseCaseRegistration @Inject constructor(private val repository: RepositorySave) {

    suspend fun registration(person: Person.PersonDataAuth) = repository.insert(person)
}