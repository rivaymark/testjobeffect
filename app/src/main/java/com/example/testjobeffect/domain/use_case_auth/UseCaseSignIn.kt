package com.example.testjobeffect.domain.use_case_auth

import com.example.testjobeffect.data.RepositorySave
import javax.inject.Inject

class UseCaseSignIn @Inject constructor(private val repository: RepositorySave) {

    suspend fun signIn(firstName: String, lastNameOrCode: String, email:String) = repository.signIn(firstName,lastNameOrCode,email)

}