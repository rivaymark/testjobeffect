package com.example.testjobeffect.domain.use_case_flow

import com.example.testjobeffect.data.RepositorySave
import com.example.testjobeffect.entities.aouth.AppAuthState
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.stateIn
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UseCaseFlowStateAuth @Inject constructor(private val repositorySave: RepositorySave){

    // Состояние авторизации пользователя в приложении ( для всех)
    //todo мейби но сомневаюсь что тебе интересно что-то кроме вошел или не вошел
    /*val stateAuth = flow<AppAuthState> { coroutineScope {
        while (isActive){
            delay(DELAY)
            emit(getState())
        }
    } }.stateIn(
        scope = CoroutineScope(Dispatchers.Default),
        started = SharingStarted.WhileSubscribed(DELAY_SUB),
        initialValue = getState()
    )*/

    fun getState() = repositorySave.getStateAuth()

    fun saveState(state: AppAuthState) = repositorySave.saveStateAuth(state)

    companion object{
        const val DELAY = 100L
        const val DELAY_SUB = 1000L
    }

}