package com.example.testjobeffect.domain.use_case_room

import com.example.testjobeffect.data.RepositorySave
import javax.inject.Inject

class UseCaseRoomFoundPerson  @Inject constructor(private val repository: RepositorySave) {

    suspend fun foundAuthorized() = repository.foundAuthorized()

}