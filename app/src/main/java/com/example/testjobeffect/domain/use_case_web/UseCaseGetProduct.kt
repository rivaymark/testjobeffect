package com.example.testjobeffect.domain.use_case_web

import com.example.testjobeffect.data.RepositoryLoad
import com.example.testjobeffect.entities.web.Product
import javax.inject.Inject

class UseCaseGetProduct @Inject constructor(private val repository: RepositoryLoad) {

    suspend fun getLatest() = repository.getLatest()

    suspend fun getFlashSale() = repository.getFlashSale()
}