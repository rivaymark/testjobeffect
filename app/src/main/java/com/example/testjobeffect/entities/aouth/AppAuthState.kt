package com.example.testjobeffect.entities.aouth

enum class AppAuthState() {
    NO_AUTHENTICATION(),        // нет аутефикации
    LOGOUT(),                   // выход
    NO_CORRECT_DATA(),          // не кореетные вводимые данные
    YOU_NEED_INPUT_PASSWORD(),  // необходимо ввести пароль
    YOU_ARE_REGISTERED(),       // Вы зарегистрированы
    YOU_ARE_NOT_REGISTERED(),   // Вы не зарегистрированы
    LOGIN(),                    // вход в аккаунт
    SIGN_IN(),                  // регистрация аккаунта и вход
    AUTHENTICATION(),           // успешная аутефикация
    ERROR(),                    // какая либо ошибка
    LOADING()                   // идет загрузка
}