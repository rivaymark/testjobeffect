package com.example.testjobeffect.entities.presentation_data

import com.example.testjobeffect.R

//README
// todo текст тоже как ключ ( INT) когда буду выводить и буду баловаться с контекстом ( пока так)
//  добавить в ресурсы
//  еще и потому что это я не был до конца уверен как я это буду делать ( это все временно т.е навсегда)
sealed interface DataConfigurationApp{

    var configurationMap: Map<String,Int> // INT не страшно так как различие в адаптерах.

    abstract class DataConfigurationForFragmentPerson:DataConfigurationApp{
        abstract override var configurationMap:  Map<String,Int>

        object DataConfigurationForFragmentPersonSettings: DataConfigurationForFragmentPerson(){
            //тип-число будет означать что нужно сделать с элементом адаптеру
            override var configurationMap: Map<String,Int> = mapOf(
                "Trade store" to IconForSettingsPerson.TRANSITION.ordinal,
                "Payment method" to IconForSettingsPerson.TRANSITION.ordinal,
                "Balance" to IconForSettingsPerson.MONEY.ordinal,
                "Trade history" to IconForSettingsPerson.TRANSITION.ordinal,
                "Restore Purchase" to IconForSettingsPerson.REFRESH.ordinal, // хз может это перезагрузка данных
                "help" to IconForSettingsPerson.HELP.ordinal,    //или  это интент или ссылка на браузер?
                "LogOut" to IconForSettingsPerson.LOGOUT.ordinal,
            )
        }
        enum class IconForSettingsPerson(val idIcon:Int){
            MONEY(R.drawable.ic_settings_credit_card),       // c текстом
            TRANSITION(R.drawable.ic_settings_credit_card),  // со стрелочкой
            HELP(R.drawable.ic_settings_help),
            LOGOUT(R.drawable.ic_settings_logout),
            REFRESH(R.drawable.ic_settings_restore),
            ERROR(R.drawable.image_avatar_sample)
            //ну или код выдумать
        }
    }

    abstract class DataConfigurationForFragmentHome:DataConfigurationApp{
        abstract override var configurationMap:  Map<String,Int>

        object DataConfigurationForFragmentHomeCategories:DataConfigurationForFragmentHome(){
            override var configurationMap:  Map<String,Int> = mapOf(
                "Phones" to R.drawable.ic_category_phone,
                "Headphones" to R.drawable.ic_category_head_phones,
                "Games" to R.drawable.ic_category_play,
                "Cars" to R.drawable.ic_category_car,
                "Furniture" to R.drawable.ic_category_bed,
                "Kids" to R.drawable.ic_category_robot,
            )
        }
        object DataConfigurationForFragmentHomeProduct:DataConfigurationForFragmentHome(){
            // количество элементов в списке
            // todo как-то особо value здесь и не понадобилось ( мб что-то лучше стоило придумать)
            override var configurationMap:  Map<String,Int> = mapOf(
                "Latest" to R.layout.sample_rv_product_for_latest,
                "Flash Sale" to R.layout.sample_rv_product_for_flash,
                "Brand" to R.layout.sample_rv_product_for_brand,
            )
        }
    }
    data class ContractCategory(
        val title:String,
        val id:Int
    )

    data class ContractSettingPerson(
        val title:String,
        val keyType:Int,
        val textEnd:String
    )
}