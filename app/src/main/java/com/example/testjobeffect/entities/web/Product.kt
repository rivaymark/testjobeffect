package com.example.testjobeffect.entities.web

interface Product {

    val category: String
    val name: String
    val price: Float
    val imageUrl: String

    interface ProductWithDiscount:Product {
        val discount:Int
    }
}