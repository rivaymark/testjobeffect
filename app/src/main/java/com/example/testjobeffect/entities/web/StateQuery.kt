package com.example.testjobeffect.entities.web

sealed interface StateQuery {

    val message: String
    val code: Int

    object OK : StateQuery {
        override val message: String get() = "Все сработало, как и ожидалось"
        val messageForNull: String get() = "Статус 200, но пришел null"
        override val code: Int = 200
    }

    object NotCorrectQuery: StateQuery {
        override val message: String get() = "неверный запрос\tЗапрос был неприемлемым, часто из-за отсутствия обязательного параметра"
        override val code: Int = 400
    }

    object NotToken: StateQuery {
        override val message: String get() = "Неавторизованный\tНедопустимый маркер доступа"
        override val code: Int = 401
    }

    object NotResources: StateQuery {
        override val message: String get() = "Запрошенный ресурс не существует"
        override val code: Int = 404
    }

    object NotCorrectScope: StateQuery {
        override val message: String get() = "Запрещено\tОтсутствуют разрешения для выполнения запроса"
        override val code: Int = 403
    }

    object SomethingWrongServer : StateQuery {
        override val message: String get() = "Что-то пошло не так с нашей стороны"
        override val code: Int = 500
    }

    object HZ : StateQuery {
        override val message: String get() = "неизвестная ошибка"
        override val code: Int = 0
    }

}