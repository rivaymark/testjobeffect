package com.example.testjobeffect.presentation.activity_load

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.example.testjobeffect.R
import com.example.testjobeffect.databinding.ActivityLoadBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ActivityLoad : AppCompatActivity() {

    private var binding: ActivityLoadBinding? = null
    private var navController: NavController? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoadBinding.inflate(layoutInflater)
        setContentView(binding?.root)
        initNav()  // навигация
        supportActionBar?.hide()  // убрать тулбар
    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
    }

    private fun initNav() {
        val navHostFragment = supportFragmentManager
            .findFragmentById(R.id.activity_load_nav_host) as NavHostFragment
        navController = navHostFragment.navController
    }
}











