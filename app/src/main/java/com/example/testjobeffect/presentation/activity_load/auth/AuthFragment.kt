package com.example.testjobeffect.presentation.activity_load.auth

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.method.PasswordTransformationMethod
import android.text.method.SingleLineTransformationMethod
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.example.testjobeffect.R
import com.example.testjobeffect.databinding.FragmentAuthBinding
import com.example.testjobeffect.entities.aouth.AppAuthState
import com.example.testjobeffect.presentation.activity_main.ActivityMain
import com.example.testjobeffect.presentation.adapters.checkEmailText
import com.example.testjobeffect.presentation.adapters.checkNormText
import com.example.testjobeffect.presentation.adapters.checkPasswordText
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@AndroidEntryPoint
class AuthFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: AuthViewModelFactory
    private val viewModel: AuthViewModel by viewModels { viewModelFactory }

    private var _binding: FragmentAuthBinding? = null
    private val binding: FragmentAuthBinding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAuthBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        hideKeyboardFrom() // убрать клавиатуру
        observers()        // обсерверы
        checkInputText()   // вкл проверку ввода текста
        clickers()         // слушатели нажатия
        viewModel.start()           // старт ( получить состояние авторизации )
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    /****************************************************/
    /*****          ВНЕШНИЙ ВИД ФРАГМЕНТА         *******/
    /****************************************************/

    private fun showSignIn() {
        binding.apply {
            fragmentAuthButtonLogginOrSign.text =getText(R.string.text_button_log_in)
            fragmentAuthTitle.text = getText(R.string.title_text_sign_in)
            fragmentAuthButtonAction.text = getText(R.string.text_button_sign_in)
            fragmentAuthEditLastName.hint = getText(R.string.text_hilt_last_name)
            fragmentAuthEditLastNameEdit.transformationMethod = SingleLineTransformationMethod.getInstance()
            /**************************/
            fragmentAuthEditLastNameEdit.removeTextChangedListener(binding.fragmentAuthEditLastNameEdit.checkPasswordText(binding.fragmentAuthEditLastName))
            fragmentAuthEditLastNameEdit.checkNormText(binding.fragmentAuthEditLastName)
            fragmentAuthEditLastNameEdit.editableText.clear()
            fragmentAuthEditLastName.isErrorEnabled = false
            /**************************/
            fragmentAuthEditLastName.isEndIconVisible = false
        }
        binding.apply {
            fragmentAuthEditEmailName.visibility = View.VISIBLE
            fragmentAuthTextAlreadyHaveAnAccount.visibility = View.VISIBLE
            fragmentAuthButtonSignGoogle.visibility = View.VISIBLE
            fragmentAuthButtonSignApple.visibility = View.VISIBLE
        }
    }   // Внешний вид для ВХОДА

    private fun showLogin(){
        binding.apply {
            fragmentAuthButtonLogginOrSign.text = getText(R.string.text_button_sign_in)
            fragmentAuthTitle.text = getText(R.string.title_text_welcome_back)
            fragmentAuthButtonAction.text = getText(R.string.text_button_login)
            fragmentAuthEditLastName.hint = getText(R.string.text_hilt_password)
            fragmentAuthEditLastNameEdit.transformationMethod = PasswordTransformationMethod.getInstance()
            /**************************/
            fragmentAuthEditLastNameEdit.removeTextChangedListener(binding.fragmentAuthEditLastNameEdit.checkNormText(binding.fragmentAuthEditLastName))
            fragmentAuthEditLastNameEdit.checkPasswordText(binding.fragmentAuthEditLastName)
            fragmentAuthEditLastNameEdit.editableText.clear()
            fragmentAuthEditLastName.isErrorEnabled = false
            /**************************/
            fragmentAuthEditLastName.isEndIconVisible = true
        }
        binding.apply {
            fragmentAuthEditEmailName.visibility = View.GONE
            fragmentAuthTextAlreadyHaveAnAccount.visibility = View.GONE
            fragmentAuthButtonSignGoogle.visibility = View.GONE
            fragmentAuthButtonSignApple.visibility = View.GONE
        }
    }  // Внешний вид для РЕГИСТРАЦИИ

    private fun message(message:String) = Snackbar.make(binding.root,message, Snackbar.LENGTH_SHORT).show()  //ПОКАЗАТЬ СООБЩЕНИЯ

    private fun checkInputText(){
        binding.fragmentAuthEditFirstNameEdit.checkNormText(binding.fragmentAuthEditFirstName)
        binding.fragmentAuthEditEmailNameEdit.checkEmailText(binding.fragmentAuthEditEmailName)
        binding.fragmentAuthEditFirstNameEdit.text
    } // проверка ввода текста

    /****************************************************/
    /*********     Развлечения с клавиатурой    *********/
    /****************************************************/

    private fun hideKeyboardFrom() {
        val imm = this.context?.applicationContext?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(binding.root.windowToken, 0)
    } // для фрагмента( убрать клавиатуру)

    /****************************************************/
    /***************         подписки     ***************/
    /****************************************************/

    private fun observers(){
        viewModel.apply {
            // СООБЩЕНИЯ ПРИ СОСТОЯНИИ АВТОРИЗАЦИИ и реагирования
            authState.onEach {
                Log.d("aaa","authState.onEach  $it ")
                when(it){
                    AppAuthState.AUTHENTICATION -> { eventAuthSuccessful() }
                    AppAuthState.NO_AUTHENTICATION -> {}
                    AppAuthState.ERROR -> {}
                    AppAuthState.LOADING -> {}
                    AppAuthState.LOGIN -> {}
                    AppAuthState.SIGN_IN -> {}
                    AppAuthState.LOGOUT -> {viewModel.justLoginOrSignIn()} //так как вход  был ранее ( Уберем регистрация и сразу вход )
                    AppAuthState.YOU_NEED_INPUT_PASSWORD ->{}
                    AppAuthState.YOU_ARE_REGISTERED -> {}
                    AppAuthState.YOU_ARE_NOT_REGISTERED -> {}
                    AppAuthState.NO_CORRECT_DATA -> {}
                }.apply {  message(it.name)}
            }.launchIn(lifecycleScope)
            /****************************************************/
            // ПОКАЗЫВАТЬ ВНЕШНИЙ ВИД
            isClickActionButtonLogin.onEach {
                if(it) showLogin()
                else showSignIn()
            }.launchIn(lifecycleScope)
            /****************************************************/
            // если позователь не уверенный и по нескольку раз переходит от одного
            //к другому то вводит данные то нет ( по сути обнулять если начинает баловаться)
            isClickTextButtonLogin.onEach {
                //то в целом проиходит)))
                // можно например очистить edit text  ( но пока хз )
                // контролирует пока viewmodel
                // здесь же кстати тайтл поменять вместо велком бек идет регистрация
            }.launchIn(lifecycleScope)
        }
    }

    /****************************************************/
    /***         инициализация слушателей нажатия     ***/
    /****************************************************/

    private fun clickers(){
        binding.apply {
            fragmentAuthButtonLogginOrSign.setOnClickListener {
                viewModel.justLoginOrSignIn()
            } /// смена ( вход или регистрация) ( при этом отменить все то что было до)
            /****************************************************/
            fragmentAuthButtonAction.setOnClickListener {
                when(viewModel.isClickActionButtonLogin.value){
                    true -> logIn()
                    false -> signIn()
                }
            } // Пользователь нажимает на кнопку хочет войти или зарегистрироваться
            /****************************************************/
            /*fragmentAuthEditLastName.setEndIconOnClickListener {
                message("нажал")
            }*/
        }
    }

    /****************************************************/
    /*********        ВЕСЕЛАЯ АВТОРИЗАЦИЯ       *********/
    /****************************************************/

    private fun signIn(){
        // ОТДАТЬ ДАННЫЕ ДЛЯ РЕГИСТРАЦИИ
        binding.apply {
            viewModel.signIn(
                fragmentAuthEditFirstNameEdit.text.toString(),
                fragmentAuthEditLastNameEdit.text.toString(),
                fragmentAuthEditEmailNameEdit.text.toString()
            )
        }
    }

    private fun logIn(){
        // ОТДАТЬ ДАННЫЕ ДЛЯ ВХОДА
        binding.apply {
          viewModel.logIn(
                fragmentAuthEditFirstNameEdit.text.toString(),
                fragmentAuthEditLastNameEdit.text.toString()
            )
        }
    }

    private fun eventAuthSuccessful(){
        // c авторизацией все круто пришел токен ( просто интент)
        val intent = Intent(requireContext().applicationContext, ActivityMain()::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK )
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK )
        startActivity(intent)
    }  // ивент ( авторизация успешна ) -> ( переход на другое активити )
}