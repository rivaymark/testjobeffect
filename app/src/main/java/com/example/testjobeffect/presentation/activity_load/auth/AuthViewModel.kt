package com.example.testjobeffect.presentation.activity_load.auth

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.testjobeffect.domain.use_case_auth.UseCaseLogIn
import com.example.testjobeffect.domain.use_case_auth.UseCaseRegistration
import com.example.testjobeffect.domain.use_case_auth.UseCaseSignIn
import com.example.testjobeffect.domain.use_case_flow.UseCaseFlowStateAuth
import com.example.testjobeffect.entities.aouth.AppAuthState
import com.example.testjobeffect.entities.aouth.Person
import com.example.testjobeffect.presentation.adapters.checkEmailText
import com.example.testjobeffect.presentation.adapters.checkNormText
import com.example.testjobeffect.presentation.adapters.checkPassword
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import javax.inject.Inject

class AuthViewModel @Inject constructor(
    private val useCaseFlowStateAuth: UseCaseFlowStateAuth,
    private val useCaseLogIn: UseCaseLogIn,
    private val useCaseSignIn: UseCaseSignIn,
    private val useCaseRegistration: UseCaseRegistration,
) : ViewModel() {

    private val job = SupervisorJob()
    private val scope = CoroutineScope( job + viewModelScope.coroutineContext + Dispatchers.Default)
    /********************************************/
    private val _isClickTextButtonLogin = MutableStateFlow<Boolean?>(null)
    val isClickTextButtonLogin = _isClickTextButtonLogin.asStateFlow()
    // пользователь хочет зарегистрироваться но его поведение надо бы контролировать
    // null - cтарт ( отменить все то что могло быть до ) ( всмысле идут данные по регистрации но пользователь передумал и решил просто войти
    // false - sign in - только имя и email ( идет регистрация)
    // true - login  - все есть ( по сути реагировать так же как и null)
    /********************************************/
    private val _isClickActionButtonLogin = MutableStateFlow<Boolean>(false)
    val isClickActionButtonLogin = _isClickActionButtonLogin.asStateFlow()
    // нажатие на кнопку Login/ вход или регистрация
    // false - sign in
    // true - login

    /********************************************/
    private val _authState = MutableSharedFlow<AppAuthState>()
    val authState =_authState.asSharedFlow()
    // состояние авторизации
    //use case интересно вошел или не вошел ( остальные состояния временные)
    /********************************************/

    fun start(){
        scope.launch {
            //_authState.emit(useCaseFlowStateAuth.getState())
            job.complete()
            job.join()
        }
    }

    fun signIn(firstName: String, lastName: String, email: String) {
        if( checkText(firstName,lastName,email,true)){
            scope.launch {
                kotlin.runCatching {
                    // cмысл: я проверяю есть ли пользователь
                    val resultSignIn = async { useCaseSignIn.signIn(firstName, lastName, email)}
                    if (resultSignIn.await() == null) {
                        // такого пользователя нет сохраним его временно и ждем пароль ( условно ждем )
                        person.email = email
                        person.lastName = lastName
                        person.firstName = firstName
                        _isClickTextButtonLogin.emit(false)
                        _authState.emit(AppAuthState.YOU_NEED_INPUT_PASSWORD)
                        _isClickActionButtonLogin.emit(!isClickActionButtonLogin.value)
                    } else {
                        // такой пользователь есть
                        _isClickActionButtonLogin.emit(true)
                        _authState.emit(AppAuthState.YOU_ARE_REGISTERED)
                    }
                }.fold(
                    onFailure = {
                        Log.d("aaa","error $it")
                        _isClickTextButtonLogin.emit(null)
                        _authState.emit(AppAuthState.ERROR)
                    },
                    onSuccess = { }
                )
                job.complete()
                job.join()
            }
        }
    }
    // Хочу зарегистрироваться
    /********************************************/

    fun logIn(firstName: String, password:String) {
        if( checkText(firstName,password,null,false)){
            scope.launch {
                kotlin.runCatching {
                    // cмысл: я проверяю есть ли пользователь с таким паролем и логином //todo наверное лучше проверить только логин
                    val resultLogIn = async { useCaseLogIn.login(firstName, password) }
                    // идет регистрация или стандартный вход?

                    when (_isClickTextButtonLogin.value) {
                        null -> { // стандартный вход
                            if (resultLogIn.await() == null) {
                                // такого пользователя нет
                                _authState.emit(AppAuthState.YOU_ARE_NOT_REGISTERED)
                            } else {
                                // такой пользователь есть стандартный вход
                                val resultUpload = async{
                                    //обновить состояние
                                    //если не обновится то все будет ок ( мы просто вернемся назад)
                                    // так как нет того кто вошел ))
                                    val data = resultLogIn.await()
                                    data!!.authorizedApp = true
                                    useCaseLogIn.update(data)
                                    true
                                }
                                if(resultUpload.await()) {
                                    useCaseFlowStateAuth.saveState(AppAuthState.AUTHENTICATION)
                                    _authState.emit(AppAuthState.AUTHENTICATION)
                                }
                                else _authState.emit(AppAuthState.ERROR) // хотя не сработает надо бы проверить
                            }
                        }
                        false -> { // идет регистрация
                            val temp = async {
                                // мы хотим получить пароль и сохранить его
                                person.password = password
                                // и зарегистрировать пользователя
                                useCaseRegistration.registration(person)
                                // пользователь зарегистрирован
                                // войти в систему
                                useCaseFlowStateAuth.saveState(AppAuthState.AUTHENTICATION)
                                true
                            }
                            if (temp.await()) _authState.emit(AppAuthState.AUTHENTICATION)
                        }
                        true -> {  // идет вход
                            _authState.emit(AppAuthState.ERROR)
                            // пока работает так же как и null
                            // todo довести до конца ( на этом этапе преполагал что необходимо 3 состояния )
                        }
                    }
                }.fold(
                    onFailure = {
                        Log.d("aaa","error $it")
                        _authState.emit(AppAuthState.ERROR)},
                    onSuccess = {}
                )
                job.complete()
                job.join()
            }
        }
    }  // Хочу зарегистрироваться
    /********************************************/

    fun justLoginOrSignIn(){
        scope.launch {
            _isClickTextButtonLogin.emit(null)
            _isClickActionButtonLogin.emit(!_isClickActionButtonLogin.value) //инвертировать!
            job.complete()
            job.join()
        }
    }//Я ничего не хочу дайте просто войти или зарегистрироваться
    /********************************************/

    private fun checkText(
        firstName: String,
        lastNameOrPassword: String,
        email: String?,
        checkLoginTrueOrSignFalse: Boolean = false
    ): Boolean {
        return if (checkLoginTrueOrSignFalse) {  // SIGNIN
            if (
                firstName.checkNormText() ||
                lastNameOrPassword.checkNormText() ||
                (email ?: "").checkEmailText()
            ) {
                /*ошибки есть*/
                scope.launch {
                    _authState.emit(AppAuthState.NO_CORRECT_DATA)
                    job.complete()
                    job.join()
                }
                false
            } else {
                /*ошибок нет */
                true
            }
        } else {  // LOGIN
            if (
                firstName.checkNormText() ||
                lastNameOrPassword.checkPassword()
            ) {
                /*ошибки есть*/
                scope.launch {
                    _authState.emit(AppAuthState.NO_CORRECT_DATA)
                    job.complete()
                    job.join()
                }
                false
            } else {
                /*ошибок нет */
                true
            }
        }
    }
    //результаты проверки текста
    /********************************************/

    companion object {
        //или мейби во флоу (для регистрации)
        val person = object : Person.PersonDataAuth {
            override var id: Int? = null
            override var firstName: String = ""
            override var lastName: String = ""
            override var email: String = ""
            override var password: String = ""
            override var authorizedApp = true  // так как происходит регистрация (если)
        }
    }
}