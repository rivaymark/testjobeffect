package com.example.testjobeffect.presentation.activity_load.auth

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import javax.inject.Inject

class AuthViewModelFactory @Inject constructor(
    private val viewModel: AuthViewModel
): ViewModelProvider.Factory{
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if( modelClass.isAssignableFrom(AuthViewModel::class.java)){
            return viewModel as T
        }
        throw IllegalArgumentException("Unknown class name")
    }
}