package com.example.testjobeffect.presentation.activity_main

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.example.testjobeffect.R
import com.example.testjobeffect.databinding.ActivityMainBinding
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationBarView.*
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ActivityMain : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding/*? = null*/
    private var navController: NavController? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding?.root)
        initNav()  // навигация
        supportActionBar?.hide()  // убрать тулбар
    }

    override fun onDestroy() {
        super.onDestroy()
        //binding = null
    }

    private fun initNav(){
        val navView : BottomNavigationView = binding!!.activityMainBottomNavigation
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.activity_main_nav_host_fragment) as NavHostFragment
        navController = navHostFragment.navController
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_home,R.id.navigation_like,R.id.navigation_basket,R.id.navigation_message,R.id.navigation_person
            )
        )
        setupActionBarWithNavController(navController!!, appBarConfiguration)
        navView.setupWithNavController(navController!!)
        navView.setBackgroundResource(R.drawable.support_round_for_bottom)
        //navView.labelVisibilityMode = LABEL_VISIBILITY_UNLABELED
        navView.isItemHorizontalTranslationEnabled = true
        // остальное в разметке

    }
}