package com.example.testjobeffect.presentation.activity_main.blank

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import javax.inject.Inject

class BlankViewModel  @Inject constructor(
    //private val useCaseGetPhoto: UseCaseGetPhoto
) : ViewModel() {

    private val job = SupervisorJob()
    private val scope = CoroutineScope(job + viewModelScope.coroutineContext + Dispatchers.IO)

    private val _load = MutableStateFlow<Boolean?>(false)
    val load = _load.asStateFlow()


}