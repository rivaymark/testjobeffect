package com.example.testjobeffect.presentation.activity_main.blank

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import javax.inject.Inject

class BlankViewModelFactory @Inject constructor(
    private val viewModel: BlankViewModel
): ViewModelProvider.Factory{
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if( modelClass.isAssignableFrom(BlankViewModel::class.java)){
            return viewModel as T
        }
        throw IllegalArgumentException("Unknown class name")
    }
}