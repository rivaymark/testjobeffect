package com.example.testjobeffect.presentation.activity_main.home

import android.os.Bundle
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewbinding.ViewBinding
import com.example.testjobeffect.R
import com.example.testjobeffect.databinding.*
import com.example.testjobeffect.entities.presentation_data.DataConfigurationApp
import com.example.testjobeffect.presentation.adapters.AdapterRVCategories
import com.example.testjobeffect.presentation.adapters.convertListInHold
import com.example.testjobeffect.presentation.adapters.convertListInItemRV
import com.example.testjobeffect.presentation.adapters.groupie_for_rv_home.AdapterGroupieItemBrand
import com.example.testjobeffect.presentation.adapters.groupie_for_rv_home.AdapterGroupieItemFlash
import com.example.testjobeffect.presentation.adapters.groupie_for_rv_home.AdapterGroupieItemLatest
import com.example.testjobeffect.presentation.adapters.groupie_for_rv_home.AdapterGroupieItemRV
import com.xwray.groupie.GroupieAdapter
import com.xwray.groupie.viewbinding.BindableItem
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@AndroidEntryPoint
class HomeFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: HomeViewModelFactory
    private val viewModel: HomeViewModel by viewModels { viewModelFactory }

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    private var adapterRVCategories: AdapterRVCategories? = null
    private var adapterRVProducts: GroupieAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(LayoutInflater.from(requireContext()),container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        /*************************/
        /***       старт       ***/
        /*************************/
        initAdaptersRV()  // инициализация адаптеров
        setColorTitle()   // настройка заголовка ( поменять цвет)
        /****************************************************/
        /***         инициализация слушателей нажатия     ***/
        /****************************************************/
        // для локации
        // для поиска ( ну использовать тот же )
        // открыть боковое меню?
        /****************************************************/
        /***************         подписки     ***************/
        /****************************************************/
        viewModel.apply {
            load.onEach {
                Log.d("aaa", "Home загрузка $it")
                when (it) {
                    true -> {
                        contentIsLoadTrue()
                        binding.fragmentHomeLoad.textErrorGone()
                        binding.fragmentHomeLoad.play()
                    }
                    false -> {
                        contentIsLoadFalse()
                        delay(200)  // дать время надуть все
                        binding.fragmentHomeLoad.stop()
                    }
                    null -> {
                        binding.fragmentHomeLoad.stop()
                        binding.fragmentHomeLoad.textErrorVisible("проверьте подключение) когда как код этого не проверяет))))")
                    }
                }
            }.launchIn(lifecycleScope)// Состояние загрузки (ошибки)
        }
    }

    override fun onStop() {
        super.onStop()
        Log.d("aaa", "Home onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("aaa", "Home загрузка onDestroy")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    /****************************************************/
    /***                Работа с контентом            ***/
    /****************************************************/

    private fun initAdaptersRV() {
        /*****************************/
        adapterRVCategories = AdapterRVCategories { data -> Unit }
        binding.fragmentHomeRvCategory.apply {
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
            adapter = adapterRVCategories
        }
        /*****************************/
        adapterRVProducts = GroupieAdapter()
        binding.fragmentHomeRvProduct.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = adapterRVProducts
        }
    }     // инициализация адаптеров

    private fun contentIsLoadTrue() {
        binding.apply {
            fragmentHomeButtonLocation.visibility = View.GONE
            fragmentHomeAvatar.visibility = View.GONE
            fragmentPersonButtonOpenMenuSide.visibility = View.GONE
            fragmentHomeTitleTrade.visibility = View.GONE
            fragmentHomeSearch.visibility = View.GONE
            fragmentHomeRvProduct.visibility = View.GONE
        }
    }  // начало работы с контентом ( когда данные получены 1 раз ) - убрать загрузку

    private fun contentIsLoadFalse() {
        /********************/
        binding.apply {
            fragmentHomeButtonLocation.visibility = View.VISIBLE
            fragmentHomeAvatar.visibility = View.VISIBLE
            fragmentPersonButtonOpenMenuSide.visibility = View.VISIBLE
            fragmentHomeTitleTrade.visibility = View.VISIBLE
            fragmentHomeSearch.visibility = View.VISIBLE
            fragmentHomeRvProduct.visibility = View.VISIBLE
        }
        adapterRVCategories?.submitList(convertToContractCategory())
        adapterRVProducts?.addAll(fillContentRVProduct())
    } // начало работы с контентом ( когда данные получены 1 раз ) - показать загрузку

    private fun setColorTitle(/**something**/){
        //todo хотел придумать что-то с принимаемыми параметрами пока так
        val text1 =  requireContext().applicationContext.getString(R.string.text_trade_by_)
        val text2 =  " bata"
        val span = SpannableStringBuilder( text1 + text2)
        val blue = ForegroundColorSpan(requireContext().getColor(R.color.blue_link))
        val black = ForegroundColorSpan(requireContext().getColor(R.color.black))
        span.setSpan(blue,(text1.length ), (text2.length + text1.length), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        span.setSpan(black,0, text1.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        binding.fragmentHomeTitleTrade.setText(span)
    }  // настройка заголовка ( поменять цвет)

    /****************************************************/
    /***                  Работа с RV                 ***/
    /****************************************************/
    ///todo наверное надо проще

    private fun <T: ViewBinding> createListProductsForRv(
        title:String,
        flowItems: Flow<List<BindableItem<T>>>,
        click:(String)->Unit = { Unit }
    ) = AdapterGroupieItemRV<T>(
        title = title,
        titleButton = resources.getString(R.string.text_button_view_all),
        flowItems =  flowItems,
        clickViewAll = { text -> click(text) }
    ) // создать список(SampleRvProductForBrandBinding) для rv Product

    private fun fillContentRVProduct() = convertListInItemRV<SampleRvListProductsBinding, BindableItem<SampleRvListProductsBinding>>(CONFIGURATION_FOR_PRODUCTS) { layout, title ->
            when (layout) {
                AdapterGroupieItemBrand.LAYOUT -> return@convertListInItemRV createListProductsForRv<SampleRvProductForBrandBinding>(title,viewModel.productBrand.map { list -> convertListInHold(list){ listData->
                    AdapterGroupieItemBrand(listData,{},{},{}) } }.stateIn(
                    scope = CoroutineScope(Dispatchers.Main + lifecycleScope.coroutineContext),
                    started = SharingStarted.Eagerly,
                    initialValue = emptyList()
                ))
                AdapterGroupieItemFlash.LAYOUT -> return@convertListInItemRV createListProductsForRv<SampleRvProductForFlashBinding>(title,viewModel.productFlash.map { list -> convertListInHold(list){ listData->
                    AdapterGroupieItemFlash(listData,{},{},{}) } }.stateIn(
                    scope = CoroutineScope(Dispatchers.Main + lifecycleScope.coroutineContext),
                    started = SharingStarted.Eagerly,
                    initialValue = emptyList()
                ))
                AdapterGroupieItemLatest.LAYOUT -> return@convertListInItemRV createListProductsForRv<SampleRvProductForLatestBinding>(title, viewModel.productLatest.map { list -> convertListInHold(list){ listData->
                    AdapterGroupieItemLatest(listData,{},{}) } }.stateIn(
                    scope = CoroutineScope(Dispatchers.Main + lifecycleScope.coroutineContext),
                    started =SharingStarted.Eagerly,
                    initialValue = emptyList()
                ))
                else -> throw Exception("Fragment Home.fillContentRVProduct - не описанное представление ")
            }
        } // создать cписки по конфигурационому файлу

    companion object {
        /**********************************************/
        /*************      для товаров     ***********/
        /**********************************************/
        val CONFIGURATION_FOR_PRODUCTS = DataConfigurationApp.DataConfigurationForFragmentHome.DataConfigurationForFragmentHomeProduct.configurationMap
        /**********************************************/
        /*************     для категорий    ***********/
        /**********************************************/
        val CONFIGURATION_FOR_CATEGORIES =
            DataConfigurationApp.DataConfigurationForFragmentHome.DataConfigurationForFragmentHomeCategories.configurationMap // получить конфигурационный файл

        fun convertToContractCategory(): List<DataConfigurationApp.ContractCategory> {
            val tempList = mutableListOf<DataConfigurationApp.ContractCategory>()
            CONFIGURATION_FOR_CATEGORIES.forEach {
                tempList.add(DataConfigurationApp.ContractCategory(title = it.key, id = it.value))
            }
            return tempList.toList()
        }  // Элементы списка соответсвуют последовательности

    }
}