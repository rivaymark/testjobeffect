package com.example.testjobeffect.presentation.activity_main.home

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.testjobeffect.domain.use_case_web.UseCaseGetProduct
import com.example.testjobeffect.entities.web.Product
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import javax.inject.Inject

class HomeViewModel  @Inject constructor(
    private val useCaseGetProduct: UseCaseGetProduct
) : ViewModel() {

    private val job = SupervisorJob()
    private val scope = CoroutineScope(job + viewModelScope.coroutineContext + Dispatchers.IO)

    //todo пока старт такой потом наверное иначе?
    init {
        getListProductsForStart()
    }

    private val _load = MutableStateFlow<Boolean?>(false)
    val load = _load.asStateFlow()

    /*****************/
    // кажется они должны быть разными
    //todo или объеденить в репозитории и сюда отправить большую пачку ? хз помоему будет монстр из данных
    // хотя если и таких списков много, как быть с ними тады хммммммммммммммммммммммм.
    // при скролле же поялвяются новые( или только одна пачка)  - под вопросом этот момент
    /****************/
    // ну или это потом превратится в page
    private val _productFlash = MutableStateFlow<List<Product.ProductWithDiscount>>(emptyList())
    val productFlash = _productFlash.asStateFlow()

    // ну или это потом превратится в page
    private val _productLatest = MutableStateFlow<List<Product>>(emptyList())
    val productLatest = _productLatest.asStateFlow()

    // ну или это потом превратится в page
    private val _productBrand = MutableStateFlow<List<Product>>(emptyList())
    val productBrand = _productBrand.asStateFlow()

    var test  = flow<Int> {  }

    private fun getListProductsForStart() {
        scope.launch {
            _load.emit(true)
            kotlin.runCatching {
                // или async
                var resultFlash = listOf<Product.ProductWithDiscount>()
                var resultLatest = listOf<Product>()
                delay(3000) //просто на лоти посмотреть

                val jobFlash = scope.launch { resultFlash = useCaseGetProduct.getFlashSale() }
                val jobLatest = scope.launch { resultLatest = useCaseGetProduct.getLatest() }

                scope.launch (Dispatchers.Default){
                    while (jobFlash.isActive || jobLatest.isActive /*добавить brand*/){Unit}
                    Log.d("aaa","Home пришли данные Flash --$resultFlash")
                    Log.d("aaa","Home пришли данные Latest --$resultLatest")
                    _productLatest.emit(resultLatest)
                    _productFlash.emit(resultFlash)
                    _productBrand.emit(forFun())
                }
            }.fold(
                onSuccess = { _load.emit(false) },
                onFailure = { _load.emit(null)  }
            )
            // тест ( временно )
            scope.launch {
                delay(5_000)
                Log.d("aaa","TEST")
                _productBrand.emit(_productBrand.value + forFun())
                _productLatest.emit(_productLatest.value  + forTest())
            }
            //this.coroutineContext.cancelChildren()
            job.complete()
            job.join()
        }
    }  // загрузить информацию о пользователе


    /***************************************/
    // просто проверить что все работает и для пустой категрии ( у которой нет api)
    fun forFun(): List<Product> {
        return listOf(
            object : Product {
                override val category: String = "падаван"
                override val name: String = "Тим"
                override val price: Float = 0f
                override val imageUrl: String = ""
            },
            object : Product {
                override val category: String = "падаван"
                override val name: String = "Тим"
                override val price: Float = 0f
                override val imageUrl: String = ""
            },
            object : Product {
                override val category: String = "падаван"
                override val name: String = "Тим"
                override val price: Float = 0f
                override val imageUrl: String = ""
            },
            object : Product {
                override val category: String = "падаван"
                override val name: String = "Тим"
                override val price: Float = 0f
                override val imageUrl: String = ""
            },
        )
    }  // Список для brand ( как бы то что получили с сервера)
    fun forTest(): List<Product> {
        return listOf(
            object : Product {
                override val category: String = "test2"
                override val name: String = "test2"
                override val price: Float = 0f
                override val imageUrl: String = ""
            },
            object : Product {
                override val category: String = "test2"
                override val name: String = "test2"
                override val price: Float = 0f
                override val imageUrl: String = ""
            },
            object : Product {
                override val category: String = "test2"
                override val name: String = "test2"
                override val price: Float = 0f
                override val imageUrl: String = ""
            },
            object : Product {
                override val category: String = "test2"
                override val name: String = "test2"
                override val price: Float = 0f
                override val imageUrl: String = ""
            },
        )
    }  // Список для brand ( как бы то что получили с сервера)

    override fun onCleared() {
        super.onCleared()

        viewModelScope.launch {
            job.complete()
            job.join()
        }
    }
}