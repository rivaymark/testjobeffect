package com.example.testjobeffect.presentation.activity_main.person

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.testjobeffect.R
import com.example.testjobeffect.databinding.FragmentPersonBinding
import com.example.testjobeffect.entities.aouth.AppAuthState
import com.example.testjobeffect.entities.presentation_data.DataConfigurationApp
import com.example.testjobeffect.presentation.activity_load.ActivityLoad
import com.example.testjobeffect.presentation.adapters.AdapterRVSettingPerson
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@AndroidEntryPoint
class PersonFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: PersonViewModelFactory
    private val viewModel: PersonViewModel by viewModels { viewModelFactory }

    private var _binding: FragmentPersonBinding? = null
    private val binding get() = _binding!!

    private var adapterRv : AdapterRVSettingPerson? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentPersonBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //инициализирую адаптер показываю что я загружаю данные о кеше ( или воозе сразу это делаю )
        initAdapter()
        viewModel.authState.onEach {
            when(it){
                AppAuthState.LOGOUT -> eventLogout()
                else->{}
            }
        }.launchIn(lifecycleScope)

        viewModel.person.onEach {
            Log.d("aaa","информация о пользователе $it")
            if (it == null){}
            else binding.fragmentPersonNamePerson.text = it.firstName
        }.launchIn(lifecycleScope)

        viewModel.result.onEach {
            if (it == true) viewModel.logOut()
        }.launchIn(lifecycleScope)

    }

    override fun onDestroy() {
        super.onDestroy()
        //todo это временно пока бд в оперативке
        //if (viewModel.authState.value != AppAuthState.LOGOUT) viewModel.logOut()
        _binding = null
    }

    private fun initAdapter(){
        adapterRv = AdapterRVSettingPerson { data -> setClick(data.keyType) }
        binding.fragmentPersonRv.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = adapterRv
        }
        adapterRv?.submitList(setContentForRV())
    }

    private fun setContentForRV(configuration: Map<String, Int> = CONFIGURATION_FOR_SETTING ): List<DataConfigurationApp.ContractSettingPerson>{
        // видимо предварительно делать запрос на получение данных баланса и добавить это хз
        val tempList = mutableListOf<DataConfigurationApp.ContractSettingPerson>()
        configuration.forEach { (key, value) ->
            if (value == DataConfigurationApp.DataConfigurationForFragmentPerson.IconForSettingsPerson.MONEY.ordinal) {
                //todo либо ждать либо заранее
                tempList.add(DataConfigurationApp.ContractSettingPerson(key,value,"$ 777"))
            }
            else tempList.add(DataConfigurationApp.ContractSettingPerson(key,value,""))
        }
        return tempList.toList()
    }

    private fun setClick(item:Int){
        when(checkType(item)){
            DataConfigurationApp.DataConfigurationForFragmentPerson.IconForSettingsPerson.MONEY -> {}
            DataConfigurationApp.DataConfigurationForFragmentPerson.IconForSettingsPerson.TRANSITION -> {}
            DataConfigurationApp.DataConfigurationForFragmentPerson.IconForSettingsPerson.HELP -> {}
            DataConfigurationApp.DataConfigurationForFragmentPerson.IconForSettingsPerson.LOGOUT -> {listenerLogout() }
            DataConfigurationApp.DataConfigurationForFragmentPerson.IconForSettingsPerson.REFRESH -> {}
            DataConfigurationApp.DataConfigurationForFragmentPerson.IconForSettingsPerson.ERROR -> {}
        }
    }

    fun checkType(key:Int):DataConfigurationApp.DataConfigurationForFragmentPerson.IconForSettingsPerson {
        return try {
            DataConfigurationApp.DataConfigurationForFragmentPerson.IconForSettingsPerson.values()
                .first { type -> type.ordinal == key }
        }
        catch (e: NoSuchElementException ){ DataConfigurationApp.DataConfigurationForFragmentPerson.IconForSettingsPerson.ERROR}
    }

    /********************************************************************************************/
    /*********************** все что связанно с логаутом ****************************************/
    /********************************************************************************************/

    private fun listenerLogout() {
        dialogAlertLogout()     //показать окно
    }  // слушатель для выхода

    private fun dialogAlertLogout(){
        AlertDialog.Builder(requireContext())
            .setTitle(requireContext().applicationContext.getString(R.string.text_alert_title_logout))
            .setMessage(R.string.text_alert_are_you_sure)
            .setPositiveButton(requireContext().applicationContext.getString(R.string.text_button_logout)) { dialogInterface, _ ->
                dialogInterface.dismiss()
                //выйти
                viewModel.emitResult(true)
            }
            .setNeutralButton(requireContext().applicationContext.getString(R.string.text_button_confidence)) { dialogInterface, _ ->
                dialogInterface.dismiss()
                //не выходить
                viewModel.emitResult(false)
            }
            .create()
            .show()
    }  // подтверждение (окно)


    private fun eventLogout(){
        // интент для возврата на лоад
        lifecycleScope.launch(Dispatchers.Main) {
            val intent = Intent(requireContext(), ActivityLoad()::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK )
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK )
            startActivity(intent)
        }
    }

    companion object{
        val CONFIGURATION_FOR_SETTING = DataConfigurationApp.DataConfigurationForFragmentPerson.DataConfigurationForFragmentPersonSettings.configurationMap
    }
}