package com.example.testjobeffect.presentation.activity_main.person

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.testjobeffect.domain.use_case_auth.UseCaseLogOut
import com.example.testjobeffect.domain.use_case_flow.UseCaseFlowStateAuth
import com.example.testjobeffect.domain.use_case_room.UseCaseRoomFoundPerson
import com.example.testjobeffect.entities.aouth.AppAuthState
import com.example.testjobeffect.entities.aouth.Person

import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import javax.inject.Inject

class PersonViewModel @Inject constructor(
    private val useCaseFlowStateAuth: UseCaseFlowStateAuth,
    private val useCaseLogOut: UseCaseLogOut,
    private val useCaseRoomFoundPerson: UseCaseRoomFoundPerson
) : ViewModel() {

    private val job = SupervisorJob()
    private val scope = CoroutineScope( job + viewModelScope.coroutineContext + Dispatchers.IO)

    private val _person = MutableStateFlow<Person.PersonDataAuth?>(null)
    val person = _person.asStateFlow()
    // получить имя пользователя который вошел в систему ( видимо за ним можно и его фото )
    // url которого я буду обнавлять в базе и сохранять
    // ну и разумеется  ( фрагмент там сам все обновит )
    /********************************************/

    /********************************************/
    private val _authState = MutableStateFlow<AppAuthState>(useCaseFlowStateAuth.getState())
    val authState =_authState.asStateFlow()
    // состояние авторизации
    /********************************************/

    private val _result = MutableStateFlow<Boolean?>(null)
    val result = _result.asStateFlow()
    // ответ пользователя если тот хочет действительно выйти
    /********************************************/

    init {
        //todo пока так
        // получить авторизованного пользователя
        scope.launch {
            val data = async { useCaseRoomFoundPerson.foundAuthorized() }
            _person.emit(data.await())
            job.complete()
            job.join()
        }
    }

    fun logOut() {
        scope.launch(Dispatchers.Default) {
            val data = person.value
            (data ?: return@launch).authorizedApp = false
            val temp = async {
                useCaseLogOut.update(data)
                useCaseFlowStateAuth.saveState(AppAuthState.LOGOUT)
                true

            }
            if (temp.await()) {
                _authState.emit(AppAuthState.LOGOUT)
            }
            job.complete()
            job.join()
        }
    }

    fun emitResult(result: Boolean?) = scope.launch() {
        _result.emit(result)
        job.complete()
        job.join()
    }
}