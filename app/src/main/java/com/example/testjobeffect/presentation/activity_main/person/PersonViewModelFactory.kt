package com.example.testjobeffect.presentation.activity_main.person

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import javax.inject.Inject

class PersonViewModelFactory @Inject constructor(
    private val viewModel: PersonViewModel
): ViewModelProvider.Factory{
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if( modelClass.isAssignableFrom(PersonViewModel::class.java)){
            return viewModel as T
        }
        throw IllegalArgumentException("Unknown class name")
    }
}