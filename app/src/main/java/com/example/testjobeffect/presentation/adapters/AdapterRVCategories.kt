package com.example.testjobeffect.presentation.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.testjobeffect.databinding.SampleRvItemCategoryBinding
import com.example.testjobeffect.entities.presentation_data.DataConfigurationApp

class AdapterRVCategories (
    private val listener:(DataConfigurationApp.ContractCategory)->Unit
) : ListAdapter< DataConfigurationApp.ContractCategory , AdapterRVCategories.Holder>(Holder.Comparator<DataConfigurationApp.ContractCategory>()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder.create(parent)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.setData(getItem(position)){ data-> listener(data)  }
    }

    class Holder(private val binding: SampleRvItemCategoryBinding) : RecyclerView.ViewHolder(binding.root) {

        fun setData(data: DataConfigurationApp.ContractCategory, listener:(DataConfigurationApp.ContractCategory)->Unit) {
            binding.sampleRvItemCategoryCustomView.fillContent(data.title,data.id)
            binding.sampleRvItemCategoryCustomView.setClick { listener(data)  }
        }

        companion object {
            fun create(parent: ViewGroup): Holder {
                return Holder(
                    SampleRvItemCategoryBinding.inflate(
                        LayoutInflater.from(parent.context), parent, false
                    )
                )
            }
        }

        class Comparator <T: DataConfigurationApp.ContractCategory> : DiffUtil.ItemCallback<T>() {
            override fun areItemsTheSame(oldItem: T, newItem: T): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: T, newItem: T): Boolean {
                return oldItem == newItem
            }
        }
    }
}
