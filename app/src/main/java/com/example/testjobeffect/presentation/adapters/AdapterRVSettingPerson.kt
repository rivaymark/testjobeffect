package com.example.testjobeffect.presentation.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.testjobeffect.databinding.SampleRvItemSettingPersonBinding
import com.example.testjobeffect.entities.presentation_data.DataConfigurationApp

class AdapterRVSettingPerson (
    private val listener:(DataConfigurationApp.ContractSettingPerson)->Unit
) : ListAdapter< DataConfigurationApp.ContractSettingPerson , AdapterRVSettingPerson.Holder>(Holder.Comparator<DataConfigurationApp.ContractSettingPerson>()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = Holder.create(parent)

    override fun onBindViewHolder(holder: Holder, position: Int) = holder.setData(getItem(position)){ data-> listener(data) }

    class Holder(private val binding: SampleRvItemSettingPersonBinding) : RecyclerView.ViewHolder(binding.root) {

        fun setData(data: DataConfigurationApp.ContractSettingPerson, listener:(DataConfigurationApp.ContractSettingPerson)->Unit) = with(binding.sampleRvItSettingPersonCustomView) {
            DataConfigurationApp.DataConfigurationForFragmentPerson.IconForSettingsPerson.values().toList()
            /***** да можно объеденить ( но мало ли что-то измениться то заменить будет проще, но хз (todo тоже уточнить)**/
            when(checkType(data.keyType)) {
                DataConfigurationApp.DataConfigurationForFragmentPerson.IconForSettingsPerson.MONEY -> {
                    removeImageEnd()
                    setTextEnd()
                    setEndText(data.textEnd)
                }
                DataConfigurationApp.DataConfigurationForFragmentPerson.IconForSettingsPerson.TRANSITION -> {
                }
                DataConfigurationApp.DataConfigurationForFragmentPerson.IconForSettingsPerson.HELP -> {
                    removeImageEnd()
                }
                DataConfigurationApp.DataConfigurationForFragmentPerson.IconForSettingsPerson.LOGOUT -> {
                    removeImageEnd()
                }
                DataConfigurationApp.DataConfigurationForFragmentPerson.IconForSettingsPerson.REFRESH -> {
                    removeImageEnd()
                }
                DataConfigurationApp.DataConfigurationForFragmentPerson.IconForSettingsPerson.ERROR -> { /*пускай все будет */}
            }.also {
                fillContent(data.title,checkType(data.keyType).idIcon)
                setClick { listener(data) }
            }
        }

        companion object{
            fun checkType(key:Int):DataConfigurationApp.DataConfigurationForFragmentPerson.IconForSettingsPerson {
                return try {
                    DataConfigurationApp.DataConfigurationForFragmentPerson.IconForSettingsPerson.values()
                        .first { type -> type.ordinal == key }
                }
                catch (e: NoSuchElementException ){ DataConfigurationApp.DataConfigurationForFragmentPerson.IconForSettingsPerson.ERROR}
            }

            fun create(parent: ViewGroup): Holder {
                return Holder(
                    SampleRvItemSettingPersonBinding.inflate(
                        LayoutInflater.from(parent.context), parent, false
                    )
                )
            }
        }

        class Comparator <T: DataConfigurationApp.ContractSettingPerson> : DiffUtil.ItemCallback<T>() {
            override fun areItemsTheSame(oldItem: T, newItem: T): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: T, newItem: T): Boolean {
                return oldItem == newItem
            }
        }
    }
}
