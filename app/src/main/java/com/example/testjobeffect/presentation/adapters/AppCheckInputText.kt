package com.example.testjobeffect.presentation.adapters

import androidx.core.text.isDigitsOnly
import androidx.core.widget.doOnTextChanged
import com.example.testjobeffect.R
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout

//const val LENGTH_STRING = 5  // как нибудь прийти к тому насколько так объявлять переменные корректно
// мейби тоже как стейты хранить хз //TODO

// Необходимо возвращать слушатели чтобы их потом удалять

/************************************************************************************************/
/************************************************************************************************/
/************************************************************************************************/
fun TextInputEditText.checkNormText(layout: TextInputLayout) =
    doOnTextChanged { text, start, before, count ->
        if (text == null || text.isEmpty()) {
            layout.isErrorEnabled = false
        }
        else {
            if (text.toString().checkNormText()) {
                layout.isErrorEnabled = true
                layout.error =
                    context.applicationContext.getText(R.string.message_error_please_input_norm_text)
            }
            else layout.isErrorEnabled = false
        }
    }

fun TextInputEditText.checkEmailText(layout: TextInputLayout) =
    doOnTextChanged { text, start, before, count ->
        if(text == null || text.isEmpty()){
            layout.isErrorEnabled = false
        }
        else {
            if (text.toString().checkEmailText()) {
                layout.isErrorEnabled = true
                layout.error =
                    context.applicationContext.getText(R.string.message_error_please_input_text_correct_email)
            }
            else layout.isErrorEnabled = false
        }
    }


fun TextInputEditText.checkPasswordText(layout: TextInputLayout) =
    doOnTextChanged { text, start, before, count ->
        if(text == null || text.isEmpty()){
            layout.isErrorEnabled = false
        }
        else{
            if (text.toString().checkPassword()) {
                //layout.isErrorEnabled = true
                layout.error = context.applicationContext.getText(R.string.message_error_please_input_password)
            }
            else layout.isErrorEnabled = false
        }

    }
/************************************************************************************************/
/************************************************************************************************/
/************************************************************************************************/

// смысл: Ошибка есть?

fun String.checkNormText():Boolean =  when(this.isNotEmpty()) {
        contains(' ') ->  true
    (length >= 5)-> false
        else ->  true
    }

fun String.checkEmailText():Boolean = when(this.isNotEmpty()) {
        isDigitsOnly() -> true
        contains(' ') -> true
        (contains("@.") || contains(".@")) -> true
        (contains('.') && contains('@')) ->  false
        else -> true
    }

fun String.checkPassword():Boolean = when(this.isNotEmpty()){
        contains(' ') -> true
        (length >= 5)-> false
        else ->  true
    }
