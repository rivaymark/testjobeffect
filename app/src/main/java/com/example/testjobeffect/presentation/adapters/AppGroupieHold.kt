package com.example.testjobeffect.presentation.adapters

import android.util.Log
import androidx.viewbinding.ViewBinding
import com.example.testjobeffect.entities.web.Product
import com.xwray.groupie.viewbinding.BindableItem

//todo или лучше так не делать хз
/*******************************************/
/*******************************************/
/*******************************************/

fun <T : ViewBinding, B : BindableItem<T>,A:Product> convertListInHold(
    listData: List<A>,
    toCreateAs: (A) -> B
): List<B> {
    val tempList = mutableListOf<B>()
    listData.forEach {
        Log.d("aaa", "convertListInHold$it")
        val result = toCreateAs(it)
        tempList.add(result)
    } //использовать так it-> AdapterGroupieItemFlash(it,...,{ Unit })
    return tempList.toList()
} //создать заполненный холдер для rv

/*******************************************/
/*******************************************/
/*******************************************/
// Сделать элемент со спискоми так по кругу - итоговый список со списками
// при создании элемента для списка в конечном списке - отдать инструкцию
// пока что инстркция = map

fun <V : ViewBinding, B : BindableItem<V>> convertListInItemRV(
    configurationListProduct: Map<String, Int> ,  // Конфигурация
    toCreateAs: (Int,String) -> B,    // какое представление и какой заголовок
): List<B> {
    val tempList = mutableListOf<B>()
    configurationListProduct.forEach { map->
        Log.d("aaa", "convertListInItemRV$map")
        val result = toCreateAs(map.value,map.key)
        tempList.add(result)
    }
    return tempList.toList()
} //Список вложенных списков


