package com.example.testjobeffect.presentation.adapters

import android.content.Context
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.GlideBuilder
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule
import com.bumptech.glide.request.RequestOptions
import com.example.testjobeffect.R

@GlideModule
class ImageLoadGlide : AppGlideModule() {
    override fun applyOptions(context: Context, builder: GlideBuilder) {
        super.applyOptions(context, builder)
        builder.apply { RequestOptions().diskCacheStrategy /*(DiskCacheStrategy.ALL)*/ }
    }
}

fun ImageView.loadImage(
    url:String,
    view: View,
    placeholder: Int = R.drawable.image_holder,
    error: Int = R.drawable.image_error
){
    GlideApp
        .with(view)
        .load(url)
        .placeholder(placeholder)// доступен после загрузки
        .error(error)
        .centerCrop()
        // установить статегию скачивания
        .into(this)
}



