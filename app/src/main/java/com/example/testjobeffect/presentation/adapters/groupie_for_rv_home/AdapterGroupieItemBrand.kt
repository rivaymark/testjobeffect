package com.example.testjobeffect.presentation.adapters.groupie_for_rv_home

import android.view.View
import com.example.testjobeffect.R
import com.example.testjobeffect.databinding.SampleRvProductForBrandBinding
import com.example.testjobeffect.databinding.SampleRvProductForLatestBinding
import com.example.testjobeffect.entities.web.Product
import com.xwray.groupie.viewbinding.BindableItem
//import com.xwray.groupie.viewbinding.GroupieViewHolder

//его возвращать списком
class AdapterGroupieItemBrand(
    private val data:Product,
    private val clickItem:(Product)->Unit?,
    private val clickButton:(Product)->Unit?,
    private val clickLike:(Product)->Unit?,
    ): BindableItem<SampleRvProductForBrandBinding>(){

    override fun getLayout(): Int = LAYOUT
    override fun initializeViewBinding(view: View) = SampleRvProductForBrandBinding.bind(view)

    override fun bind(viewBinding: SampleRvProductForBrandBinding, position: Int)= with(viewBinding.sampleRvProductForBrand){
        removeLike()
        removeSale()
        removeType()
        removeTag()
        iAmFun()
    }

    companion object{
        const val LAYOUT = R.layout.sample_rv_product_for_brand
    }
}