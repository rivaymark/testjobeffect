package com.example.testjobeffect.presentation.adapters.groupie_for_rv_home

import android.view.View
import com.example.testjobeffect.R
import com.example.testjobeffect.databinding.SampleRvProductForFlashBinding
import com.example.testjobeffect.databinding.SampleRvProductForLatestBinding
import com.example.testjobeffect.entities.web.Product
import com.xwray.groupie.viewbinding.BindableItem
//import com.xwray.groupie.viewbinding.GroupieViewHolder

//его возвращать списком
class AdapterGroupieItemFlash(
    private val data:Product.ProductWithDiscount,
    private val clickItem:(Product)->Unit?,
    private val clickButton:(Product)->Unit?,
    private val clickLike:(Product)->Unit?,
    ): BindableItem<SampleRvProductForFlashBinding>(){

    override fun getLayout(): Int =  LAYOUT
    override fun initializeViewBinding(view: View) = SampleRvProductForFlashBinding.bind(view)

    override fun bind(viewBinding: SampleRvProductForFlashBinding, position: Int)= with(viewBinding.sampleRvProductForFlash){
        fillContentWithCheck(data)
        setClickPlus { clickButton(data) }
        setClickItem { clickItem(data) }
        setClickLike { clickLike(data) }
    }
    companion object{
        const val LAYOUT = R.layout.sample_rv_product_for_flash
    }
}