package com.example.testjobeffect.presentation.adapters.groupie_for_rv_home

import android.view.View
import com.example.testjobeffect.R
import com.example.testjobeffect.databinding.SampleRvProductForLatestBinding
import com.example.testjobeffect.entities.web.Product
import com.xwray.groupie.viewbinding.BindableItem
//import com.xwray.groupie.viewbinding.GroupieViewHolder

//его возвращать списком
class AdapterGroupieItemLatest(
    private val data:Product,
    private val clickItem:(Product)->Unit?,
    private val clickButton:(Product)->Unit?,
): BindableItem<SampleRvProductForLatestBinding>(){

    override fun getLayout(): Int = R.layout.sample_rv_product_for_latest
    override fun initializeViewBinding(view: View): SampleRvProductForLatestBinding = SampleRvProductForLatestBinding.bind(view)

    override fun bind(viewBinding: SampleRvProductForLatestBinding, position: Int)= with(viewBinding.sampleRvProductForLatest){
        removeSale()
        removeAvatar()
        removeLike()
        removeType()
        setSmallTag()
        setSmallPrice()
        fillContentWithCheck(data)
        setClickPlus { clickButton(data) }
        setClickItem { clickItem(data) }
    }

    companion object{
        const val LAYOUT = R.layout.sample_rv_product_for_latest
    }

}