package com.example.testjobeffect.presentation.adapters.groupie_for_rv_home

import android.util.Log
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewbinding.ViewBinding
import com.example.testjobeffect.R
import com.example.testjobeffect.databinding.SampleRvListProductsBinding
import com.example.testjobeffect.databinding.SampleRvProductForBrandBinding
import com.example.testjobeffect.databinding.SampleRvProductForFlashBinding
import com.example.testjobeffect.databinding.SampleRvProductForLatestBinding
import com.example.testjobeffect.entities.web.Product
import com.example.testjobeffect.presentation.activity_main.home.HomeFragment
import com.example.testjobeffect.presentation.adapters.AppLinearLayoutManager
import com.example.testjobeffect.presentation.adapters.convertListInHold
import com.example.testjobeffect.presentation.adapters.convertListInItemRV
import com.xwray.groupie.GroupieAdapter
import com.xwray.groupie.viewbinding.BindableItem
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*

class AdapterGroupieItemRV< T: ViewBinding>(
    private val title:String,
    private val titleButton:String,
    private val flowItems: Flow<List<BindableItem<T>>>,
    private val clickViewAll:(String)->Unit,
): BindableItem<SampleRvListProductsBinding>(){

    private var adapterRVV: GroupieAdapter? = null

    override fun initializeViewBinding(view: View)  = SampleRvListProductsBinding.bind(view)

    override fun getLayout(): Int = LAYOUT

    override fun bind(viewBinding: SampleRvListProductsBinding, position: Int) {
        viewBinding.root.apply {
            fillContent(title){ clickViewAll(title) }
            initAdapter { rv->
                adapterRv = GroupieAdapter()
                rv.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                rv.adapter = adapterRv
                flowItems.onEach {
                    if (it.isNotEmpty()) adapterRv!!.update( it )
                }.stateIn(
                    scope = CoroutineScope(Dispatchers.Main),
                    started = SharingStarted.Eagerly,
                    initialValue = emptyList()
                )
            }
        }
    }


    companion object {
        const val LAYOUT = R.layout.sample_rv_list_products
    }
}