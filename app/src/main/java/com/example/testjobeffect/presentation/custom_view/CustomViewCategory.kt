package com.example.testjobeffect.presentation.custom_view

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.example.testjobeffect.R
import com.example.testjobeffect.databinding.CustomViewCategoryBinding

class CustomViewCategory @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
) : LinearLayout(context, attrs, defStyleAttr) {

    private var binding: CustomViewCategoryBinding

    init {
        //binding = CustomViewCategoryBinding.inflate(LayoutInflater.from(context))
        //addView(binding.root)
        val root = inflate(context, R.layout.custom_view_category, this)
        binding = CustomViewCategoryBinding.bind(root)
    }

    fun fillContent(title:String,idIcon:Int){
        binding.cusomViewCategoryText.text = title
        binding.cusomViewCategoryImage.setImageDrawable(ContextCompat.getDrawable(context,idIcon))
    }

    fun setClick(listener:()->Unit):Unit = binding.root.setOnClickListener { listener() }
}