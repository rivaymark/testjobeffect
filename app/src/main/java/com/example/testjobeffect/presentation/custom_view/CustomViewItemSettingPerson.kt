package com.example.testjobeffect.presentation.custom_view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import androidx.core.content.res.ResourcesCompat
import com.example.testjobeffect.R
import com.example.testjobeffect.databinding.CustomViewItemSettingPersonBinding

class CustomViewItemSettingPerson @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
) : LinearLayout(context, attrs, defStyleAttr) {

    private var binding: CustomViewItemSettingPersonBinding

    init {
        //binding = CustomViewItemSettingPersonBinding.inflate(LayoutInflater.from(context))
        //addView(binding.root)
        val root = inflate(context, R.layout.custom_view_item_setting_person, this)
        binding = CustomViewItemSettingPersonBinding.bind(root)
    }

    fun fillContent(title:String,idIcon:Int){
        binding.customViewItemSettingPersonTitle.text = title
        binding.customViewItemSettingPersonImage.setImageDrawable(ResourcesCompat.getDrawable(context.resources,idIcon,null))
    }


    fun setClick(listener:()->Unit):Unit = binding.root.setOnClickListener {listener()}
    fun setEndText(text:String) {binding.customViewItemSettingPersonValue.text = text}
    fun setTextEnd(){ binding.customViewItemSettingPersonValue.visibility = View.VISIBLE}
    fun removeImageEnd(){ binding.customViewItemSettingPersonButton.visibility = View.GONE}

}