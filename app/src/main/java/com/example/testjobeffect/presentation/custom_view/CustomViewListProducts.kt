package com.example.testjobeffect.presentation.custom_view

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.example.testjobeffect.R
import com.example.testjobeffect.databinding.CustomViewListProductsBinding
import com.example.testjobeffect.entities.web.Product
import com.example.testjobeffect.presentation.adapters.groupie_for_rv_home.AdapterGroupieItemLatest
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.viewbinding.BindableItem
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.stateIn

class CustomViewListProducts  @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
) : LinearLayout(context, attrs, defStyleAttr) {

    private var binding: CustomViewListProductsBinding

    var adapterRv: GroupAdapter<GroupieViewHolder>? = null

    init {
        //binding = CustomViewListProductsBinding.inflate(LayoutInflater.from(context))
        //addView(binding.root)
        val root = inflate(context, R.layout.custom_view_list_products, this)
        binding = CustomViewListProductsBinding.bind(root)
    }

    fun initAdapter(adapter:(RecyclerView)-> Unit) = adapter(binding.customViewListProductsRv)

    fun fillContent(
        title: String,
        textButton: String = context.resources.getString(R.string.text_button_view_all),
        click: () -> Unit
    ) = with(binding) {
        customViewListProductsButtonViewAll.apply {
            text = textButton
            setOnClickListener { click() }
        }
        customViewListProductsTitle.text = title
    }

    fun <T : ViewBinding> addElement(flow:Flow<List<BindableItem<T>>>) {
        flow.onEach {
            Log.e("aaa", "adapter ${adapterRv == null} bind $it  ")
            adapterRv!!.addAll(it)
        }.stateIn(
            scope = CoroutineScope(Dispatchers.Main),
            started = SharingStarted.Eagerly,
            initialValue = emptyList()
        )
    }
}