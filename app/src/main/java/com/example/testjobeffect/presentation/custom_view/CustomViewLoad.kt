package com.example.testjobeffect.presentation.custom_view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import com.airbnb.lottie.LottieDrawable
import com.example.testjobeffect.R
import com.example.testjobeffect.databinding.CustomViewLoadBinding

class CustomViewLoad @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
) : FrameLayout(context, attrs, defStyleAttr) {

    private var binding: CustomViewLoadBinding

    init {
        //binding = CustomViewLoadBinding.inflate(LayoutInflater.from(context))
        //addView(binding.root)
        val root = inflate(context, R.layout.custom_view_load, this)
        binding = CustomViewLoadBinding.bind(root)
    }

    fun play() = binding.customViewLoadLottie.apply {
        visibility = View.VISIBLE
        repeatCount = LottieDrawable.INFINITE
        playAnimation()
    }

    fun stop() = binding.customViewLoadLottie.apply {
        visibility = View.GONE
        pauseAnimation()
    }

    fun textErrorVisible(text: String) {
        binding.customViewLoadErrorText.visibility = View.VISIBLE
        binding.customViewLoadErrorText.text = text
    }

    fun textErrorGone() {
        binding.customViewLoadErrorText.visibility = View.GONE
    }
}