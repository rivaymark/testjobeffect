package com.example.testjobeffect.presentation.custom_view

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.testjobeffect.R
import com.example.testjobeffect.databinding.CustomViewProductBinding
import com.example.testjobeffect.entities.web.Product
import com.example.testjobeffect.presentation.adapters.loadImage

class CustomViewProduct @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private var binding: CustomViewProductBinding

    init {
        //binding =CustomViewProductBinding.inflate(LayoutInflater.from(context))
        //addView(binding.root)
        val root = inflate(context, R.layout.custom_view_product, this)
        binding = CustomViewProductBinding.bind(root)
    }
    //todo блин или лучше сделать если будет проще методов тьма...

    /*** что-то изменить  ***/
    fun setSmallTag() { binding.customViewProductTag.setTextSize(TypedValue.COMPLEX_UNIT_SP, 7f) }
    fun setSmallPrice() { binding.customViewProductPrice.setTextSize(TypedValue.COMPLEX_UNIT_SP, 7f)}
    /*** что-то убрать ***/
    fun removeLike() { binding.customViewProductArcLike.visibility = View.GONE}
    fun removeAvatar() { binding.customViewProductArcAvatar.visibility = View.GONE}
    fun removeType() { binding.customViewProductType.visibility = View.GONE}
    fun removeSale() { binding.customViewProductArcSale.visibility = View.GONE}
    fun removeTag() { binding.customViewProductTag.visibility = View.GONE}

    /*** как-то реагировать ***/

    fun setClickLike(click:()->Unit) = binding.customViewProductArcLike.setOnClickListener { click() }
    fun setClickPlus(click:()->Unit) = binding.customViewProductArcPlus.setOnClickListener { click() }
    fun setClickItem(click:()->Unit) = binding.root.setOnClickListener { click()}

    /*** Контент ***/
    
    fun fillContentWithCheck(data: Product)=with(binding){
        if (data is Product.ProductWithDiscount) {
            fillContentAsWithSale(data)
            return
        }
        fillContentAsProduct(data)
    }  //как его заполнить ( плохо что он знает )

    private fun fillContentAsWithSale(data: Product.ProductWithDiscount)=with(binding) {
        customViewProductSale.text  = data.discount.toString() +"% off"  //мб как настройка
        customViewProductTag.text = data.category
        customViewProductPrice.text = "\$"+ data.price  //мб как настройка
        customViewProductImage.loadImage(data.imageUrl,root)
        customViewProductName.text = data.name
        customViewProductName.textSize = 12f
        customViewProductAvatar.loadImage("null",root, error = R.drawable.image_avatar_sample)
    }

    private fun fillContentAsProduct(data: Product)=with(binding) {
        customViewProductTag.text = data.category
        customViewProductPrice.text = "\$"+ data.price  //мб как настройка
        customViewProductImage.loadImage(data.imageUrl,root)
        customViewProductName.text = data.name
        customViewProductAvatar.loadImage("null",root, error = R.drawable.image_avatar_sample)
    }

    fun iAmFun()=with(binding) {
        customViewProductPrice.text = "падаван"
        customViewProductImage.loadImage("null",root, error = R.drawable.image_avatar_sample)
        customViewProductName.text = "Тимофей"
        customViewProductAvatar.loadImage("null",root, error = R.drawable.image_avatar_sample)
    }
}